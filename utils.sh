#!/usr/bin/env bash

###########
## Utils ##
###########
## Message formatting ##

bold=$(tput bold)
underline=$(tput sgr 0 1)
reset=$(tput sgr0)

red=$(tput setaf 1)
green=$(tput setaf 76)
blue=$(tput setaf 38)
yellow=$(tput setaf 11)

## Print formatted messages ##
function info() {
    echo -e "\n${blue}${*} ${reset} \n"
}

function warning() {
    printf "\n %s %s %s \n" "${yellow}" "${*}" "${reset}"
}

function success() {
    printf "\n%s\xE2\x9C\x94 %s %s \n" "${green}" "${*}" "${reset}"
}

function error() {
    printf "\n%s%s\xe2\x9c\x95 %s %s \n" "${bold}" "${red}" "${*}" "${reset}"
}

function underline() {
     echo -e "\n ${underline} ${*} ${reset} \n"
}

function note() {
    echo -e "\n ${underline}${bold}${blue}Note:${reset}${blue} ${*} ${reset} \n"
}

## $1=command to check
## returns command path
function _exists() {
  # shellcheck disable=SC2086
  command -v ${1} > /dev/null 2>&1
}

# log ${1} to file with formatted date and time
function _log_to_file() {
    echo "$(date "+%H:%M:%S   %d/%m/%y")" "${1}" >> "${LOGFILE}"
}