#!/usr/bin/env bash
DOTFILES="${HOME}/.dotfiles"
MIN_BASH_VERSION=4
CURRENT_OS=$(uname | tr '[:upper:]' '[:lower:]')
BASH_VERSION=$(bash -c 'echo ${BASH_VERSINFO[0]}')
supported_distributions=("ubuntu" "fedora" "centos")
supported_operating_systems=("linux" "darwin")
required_packages=(
  "zsh"
  "zplug"
  "peco"
  "curl"
  "wget"
  "fzf"
  "hcloud-cli"
  "python3-distutils"
  "python3-pip"
  "trash-cli"
  "autojump"
  "fonts-firacode"
)

## Message formatting ##
bold=$(tput bold)
reset=$(tput sgr0)
red=$(tput setaf 1)
green=$(tput setaf 76)
blue=$(tput setaf 38)
yellow=$(tput setaf 11)
## Print formatted messages ##
function info() {
  echo -e "\n${blue}${*} ${reset}"
}
function warning() {
  printf "\n%s%s%s\n" "${yellow}" "${*}" "${reset}"
}
function success() {
  printf "\n%s\xE2\x9C\x94 %s %s \n" "${green}" "${*}" "${reset}"
}
function error() {
  printf "\n%s%s\xe2\x9c\x95 %s %s \n" "${bold}" "${red}" "${*}" "${reset}"
}
function _exists() {
  # shellcheck disable=SC2086
  command -v ${1} >/dev/null 2>&1
}

# Because of how Bash works, return 1 to specify false
function ask() {
  local answer
  info "${1} [y/N]"
  read -r answer
  if [[ ! ${answer} =~ ^[Yy]$ ]]; then return 1; fi
}

################
## Main steps ##
################
function __main__() {
  init_check
  configure_package_manager
  welcome
  install_dependencies
  install_extra_packages
  set_files
  configure_git
  setup_shell
  setup_success
}

function display_logo() {
  cat <<EOF
============================================================================

         ██████╗  ██████╗ ████████╗███████╗██╗██╗     ███████╗███████╗
         ██╔══██╗██╔═══██╗╚══██╔══╝██╔════╝██║██║     ██╔════╝██╔════╝
         ██║  ██║██║   ██║   ██║   █████╗  ██║██║     █████╗  ███████╗
         ██║  ██║██║   ██║   ██║   ██╔══╝  ██║██║     ██╔══╝  ╚════██║
      ██╗██████╔╝╚██████╔╝   ██║   ██║     ██║███████╗███████╗███████║
      ╚═╝╚═════╝  ╚═════╝    ╚═╝   ╚═╝     ╚═╝╚══════╝╚══════╝╚══════╝
                       modified by @andreas.poletti
                        inspired by @denysdovhan
============================================================================
EOF
}

#######################################
## Check bash version and current OS ##
##    clone whole project if not     ##
##             installed             ##
#######################################
function init_check() {
  if [[ ! ${supported_operating_systems[*]} =~ ${CURRENT_OS} ]]; then
    error "Your OS (${CURRENT_OS}) is not supported. Supported operating systems are: ${supported_operating_systems[*]}"
    exit 1
  fi
  if [[ ${BASH_VERSION} -lt ${MIN_BASH_VERSION} ]]; then
    error "You are using an outdated version of bash. Please upgrade it. \n Minimum supported version: ${MIN_BASH_VERSION}"
    exit 1
  fi
  if [[ ! -d "${DOTFILES}" ]]; then
    _exists git || {
      error "Error: GIT not installed"
      exit 1
    }
    info "Cloning dotfiles repo to ~/.dotfiles"
    git clone https://gitlab.com/andreas.poletti/dotfiles.git "${DOTFILES}"
  fi
}

function welcome() {
  display_logo
  info "Welcome to my dotfiles installer! This script will guide you through the setup."
  ask "Would you like to install those dotfiles?" || {
    info "Cancelling..."
    exit 1
  }
}

function configure_package_manager() {
  case ${CURRENT_OS} in
  "linux") configure_linux_package_manager ;;
  "darwin") configure_mac_package_manager ;;
  esac
}

function configure_mac_package_manager() {
  if ! _exists "brew"; then
    error "Please install brew"
    exit 1
  fi
  package_manager=$(which brew)
}

function configure_linux_package_manager() {
  if [ -f /etc/os-release ]; then
    . /etc/os-release
    local current_distro
    current_distro=$(echo ${NAME} | tr '[:upper:]' '[:lower:]')
  fi
  case ${current_distro} in
  "ubuntu" | "debian") package_manager=$(which apt-get) ;;
  "fedora") package_manager=$(which dnf) ;;
  "centos") package_manager=$(which yum) ;;
  *)
    error "Distro not supported. Supported distros: ${supported_distributions[*]})"
    exit 1
    ;;
  esac
}

function install_dependencies() {
  info "The following will be installed on your PC:"
  for i in "${required_packages[@]}"; do
    echo "${i}"
  done
  ask "Is it ok?"
  sudo "${package_manager}" install "${required_packages[@]}"
}

function install_extra_packages() {
  ask "Extra packages: docker, nodeJS. Would you like to install them?" || return
  _exists docker || {
    install_docker
    install_docker_compose
  }
  _exists node || {
    curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
    sudo "${package_manager}" install nodejs && fix_npm_permissions
  }
}

function set_files() {
  info "Creating symlinks"
  ln -sfv "${DOTFILES}"/home/.zshrc "${HOME}"/.zshrc
  ln -sfv "${DOTFILES}"/home/tilda_config "${HOME}"/.config/tilda/config_0
  ln -sfv "${DOTFILES}"/home/.dir_colors "${HOME}"/.dir_colors
  info "Copying git config"
  cp --verbose "${DOTFILES}"/home/.gitconfig "${HOME}"/.gitconfig
}

function fix_npm_permissions() {
  if [[ ! -d ${HOME}/.npm-global ]]; then
    mkdir ~/.npm-global
  fi
  npm config set prefix "${HOME}/.npm-global"
}

function install_docker() {
  curl -fsSL https://get.docker.com | bash
  sudo usermod -aG docker "${USER}"
}

function install_docker_compose() {
  sudo curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose
}

function configure_git() {
  ask "Would you like to configure git now?" || return
  local git_email
  local git_name
  read -rp "Type in commit email: " git_email
  git config -f ~/.gitlocal user.email "${git_email}" && success "Configured git email."
  read -rp "Type in commit name: " git_name
  git config -f ~/.gitlocal user.name "${git_name}" && success "Configured git commit name."
}

function setup_shell() {
  ask "Would you like to set zsh as the default shell?" || return
  chsh -s "$(which zsh)"
}

function setup_success() {
  success "You successfully set up my dotfiles! Feel free to contribute if you have any ideas."
}

if [[ ! $1 ]]; then
  __main__
else
  $1
fi
